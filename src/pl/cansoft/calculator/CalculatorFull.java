package pl.cansoft.calculator;

import java.util.Scanner;

public class CalculatorFull {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Jaki typ operacji chciałbyś wykonać?");
        System.out.println("A - dodawanie");
        System.out.println("B - odejmowanie");
        System.out.println("C - mnożenie");
        System.out.println("D - dzielenie");
        System.out.println("E - potęgowanie");
        System.out.println("F - pierwiastek kwadratowy");
        System.out.print("Wpisz opcję: ");

        String type;
        try {
            type = scan.nextLine();
            switch (type) {
                case "A":
                    add();
                    break;
                case "B":
                    minus();
                    break;
                case "C":
                    multiply();
                    break;
                case "D":
                    divide();
                    break;
                case "E":
                    pow();
                    break;
                case "F":
                    sqrt();
                    break;
                default:
                    throw new Exception();
            }
        } catch (Exception exception) {
            System.out.println("Typ operacji nie został rozpoznany.");
            main(args);
            return;
        }
    }

    private static void add() {
        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj argument 1: ");
        float firstArg;
        try {
            firstArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            add();
            return;
        }

        System.out.print("Podaj argument 2: ");
        float secondArg;
        try {
            secondArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            add();
            return;
        }

        float result = firstArg + secondArg;

        System.out.print("Wynik działania to: ");
        System.out.println(result);
    }

    private static void minus() {
        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj argument 1: ");
        float firstArg;
        try {
            firstArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            minus();
            return;
        }

        System.out.print("Podaj argument 2: ");
        float secondArg;
        try {
            secondArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            minus();
            return;
        }

        float result = firstArg - secondArg;

        System.out.print("Wynik działania to: ");
        System.out.println(result);
    }

    private static void multiply() {
        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj argument 1: ");
        float firstArg;
        try {
            firstArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            multiply();
            return;
        }

        System.out.print("Podaj argument 2: ");
        float secondArg;
        try {
            secondArg = scan.nextFloat();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            multiply();
            return;
        }

        float result = firstArg * secondArg;

        System.out.print("Wynik działania to: ");
        System.out.println(result);
    }

    private static void divide() {
        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj argument 1: ");
        int firstArg;
        try {
            firstArg = scan.nextInt();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            divide();
            return;
        }

        System.out.print("Podaj argument 2: ");
        int secondArg;
        try {
            secondArg = scan.nextInt();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą rzeczywistą");
            divide();
            return;
        }

        int result;
        try {
            result = firstArg / secondArg;
        } catch (ArithmeticException exception) {
            System.out.println("Nie wolno dzielić przez zero");
            divide();
            return;
        }

        System.out.print("Wynik działania to: ");
        System.out.println(result);
    }

    private static void pow() {
        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj argument 1: ");
        int firstArg;
        try {
            firstArg = scan.nextInt();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą całkowitą");
            pow();
            return;
        }

        System.out.print("Podaj argument 2: ");
        int secondArg;
        try {
            secondArg = scan.nextInt();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą całkowitą");
            pow();
            return;
        }

        double result = Math.pow(firstArg, secondArg);

        System.out.print("Wynik działania to: ");
        System.out.println(result);
    }

    private static void sqrt() {
        Scanner scan = new Scanner(System.in);

        System.out.print("Podaj argument: ");
        int arg;
        try {
            arg = scan.nextInt();
        } catch (Exception exception) {
            System.out.println("Podany argument nie jest liczbą całkowitą");
            sqrt();
            return;
        }

        double result = Math.sqrt(arg);

        System.out.print("Wynik działania to: ");
        System.out.println(result);
    }
}
