package pl.cansoft.stream;

import java.math.BigDecimal;
import java.util.Optional;

public class OptionalMain {

    public static void main(String[] args) {

        // Consumer<String> consumer = name -> System.out.println("Znaleziono tytuł: " + name);
        // Runnable runnable = () -> System.out.println("Ups! Nie ma żadnej gry");

        var b1 = new BoardGame("Codenames", 8.12, new BigDecimal("129.95"), 2, 4);
        Optional<BoardGame> bg1 = Optional.of(b1);
        bg1
            .map(bg -> bg.name)
            // .ifPresentOrElse(consumer, runnable);
            .ifPresentOrElse(
                name -> System.out.println("Znaleziono tytuł: " + name),
                () -> System.out.println("Ups! Nie ma żadnej gry")
            );

        Optional<BoardGame> bg2 = Optional.empty();
        // Supplier<String> fun = () -> "Domyślna gra";

        var test = bg2
            .map(bg -> bg.name)
            .orElse("Domyślna gra");
            // .orElseGet(() -> "Domyślna gra");
            // .orElseGet(fun);

        System.out.println(test);
    }
}
