package pl.cansoft.stream;

import java.util.ArrayList;
import java.util.LinkedList;

public class NoStream {

    public static void main(String[] args) {

        var names = new ArrayList<String>();
        names.add("Damian");
        names.add("Asia");
        names.add("Katarzyna");
        names.add("Dorota");
        names.add("Basia");
        names.add("Roman");
        names.add("Bogumiła");
        names.add("Andrzej");

        var females = new LinkedList<String>();
        for (var name : names) {
            if (name.endsWith("a") && name.length() <= 6 &&
                name.length() > 4 && name.startsWith("D")) {
                name = name.toUpperCase();
                females.add(name);
            }
        }

        System.out.println(females);
    }
}
