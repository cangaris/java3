package pl.cansoft.stream;

import java.util.ArrayList;

public class WithStream {

    public static void main(String[] args) {

        var names = new ArrayList<String>();
        names.add("Damian");
        names.add("Asia");
        names.add("Katarzyna");
        names.add("Dorota");
        names.add("Daria");
        names.add("Basia");
        names.add("Roman");
        names.add("Bogumiła");
        names.add("Andrzej");

        var females = names.stream()
            .limit(5)
            .filter(name -> name.length() > 4)
            .filter(name -> name.length() <= 6)
            .filter(name -> name.endsWith("a"))
            .filter(name -> name.startsWith("D"))
            .map(name -> name.toUpperCase())
            .toList();

        System.out.println(names);
        System.out.println(females);
    }
}
