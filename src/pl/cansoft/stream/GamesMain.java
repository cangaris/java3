package pl.cansoft.stream;

import java.math.BigDecimal;
import java.util.Arrays;

class BoardGame {
    final String name;
    final double rating;
    final BigDecimal price;
    final int minPlayers;
    final int maxPlayers;

    public BoardGame(String name, double rating, BigDecimal price, int minPlayers, int maxPlayers) {
        this.name = name;
        this.rating = rating;
        this.price = price;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
    }
}

public class GamesMain {

    public static void main(String[] args) {

        // Zadanie 1 - przefiltrować gry tylko na literę T
        // Zadanie 2 - o ratingu większym niż 8
        // Zadanie 3 - cena mniejsza niż 300
        // Zadanie 4 - z możliwością single player
        // Zadanie 5 - zmapować tylko do tytułów (name) List<BoardGame> na List<String>

        var games = Arrays.asList(
            new BoardGame("Terraforming Mars", 8.38, new BigDecimal("123.49"), 1, 5),
            new BoardGame("Codenames", 7.82, new BigDecimal("64.95"), 2, 8),
            new BoardGame("Puerto Rico", 8.07, new BigDecimal("149.99"), 2, 5),
            new BoardGame("Terra Mystica", 8.26, new BigDecimal("252.99"), 2, 5),
            new BoardGame("Scythe", 8.3, new BigDecimal("314.95"), 1, 5),
            new BoardGame("Power Grid", 7.92, new BigDecimal("145"), 2, 6),
            new BoardGame("7 Wonders Duel", 8.15, new BigDecimal("109.95"), 2, 2),
            new BoardGame("Dominion: Intrigue", 7.77, new BigDecimal("159.95"), 2, 4),
            new BoardGame("Patchwork", 7.77, new BigDecimal("75"), 2, 2),
            new BoardGame("The Castles of Burgundy", 8.12, new BigDecimal("129.95"), 2, 4)
        );

        var res = games.stream()
            .filter(boardGame -> boardGame.name.startsWith("T"))
            .filter(boardGame -> boardGame.rating > 8)
            .filter(boardGame -> boardGame.price.compareTo(BigDecimal.valueOf(300)) < 0) // lub boardGame.price.floatValue < 300
            .filter(boardGame -> boardGame.minPlayers == 1)
            .map(boardGame -> boardGame.name)
            .toList();

        System.out.println(res);
    }
}
