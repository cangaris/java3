package pl.cansoft.stream;

import java.util.List;

public class StreamNumbers {

    public static void main(String[] args) {

        List<Integer> all = List.of(1,2,3,4,5,6,7,8,9);

        var results = all.stream()
            .filter(integer -> integer % 2 == 0) // weź tylko parzyste
            .map(integer -> integer * 3) // zmodyfikuj każdy x3
            .toList();

        System.out.println(all);
        System.out.println(results);
    }
}
