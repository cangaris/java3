package pl.cansoft.stream;

import java.util.List;

public class Names {

    public static void main(String[] args) {

        var names = List.of("Damian", "ela", "Ania", "Gosia", "Zosia", "Hania", "Piotr");

        // https://pl.wikipedia.org/wiki/ASCII
        var namesFiltered = names.stream()
            // jak przefiltrować tylko od D do G?
            .filter(name -> name.charAt(0) >= 68 && name.charAt(0) <= 71)
            .toList();

        System.out.println(namesFiltered);
    }
}

