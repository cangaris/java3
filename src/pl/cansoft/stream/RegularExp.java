package pl.cansoft.stream;

import java.util.List;

public class RegularExp {

    public static void main(String[] args) {

        List<String> names = List.of(
            "Damian", "Zosia", "Piotr",
            "@67454", "7eer###", "ania",
            "1", "***K", "Gosia"
        );

        /**
         * https://regexr.com/ - stronka do wyrażeń regularnych
         */
        var result = names.stream()
            .filter(name -> name.matches("[A-Z][a-z]{2,}"))
            .toList();

        System.out.println(result);
    }
}

