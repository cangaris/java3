package pl.cansoft.vehicles;

public class Main {

    public static void main(String[] args) {

        Vehicle v = new Vehicle();
        Truck t = new Truck();

        Car c = new Car();
        c.setName("Test2");
        c.showName();

        v.setName("Test1");
        v.showName();

        t.setName("Test3");
        t.showName();
    }
}
