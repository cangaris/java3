package pl.cansoft.tanks;

public class DoubleBarrelTank extends Tank {
    int track = 2;

    public DoubleBarrelTank() {
        super(2);
    }

    @Override
    public String toString() {
        return "DoubleBarrelTank{" +
            "track=" + track +
            "}";
    }
}
