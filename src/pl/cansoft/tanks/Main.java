package pl.cansoft.tanks;

public class Main {

    public static void main(String[] args) {

        Tank t1 = new Tank(5, 7, 100);
        Tank t2 = new Tank(2,2, 150, 25);
        Tank t3 = new Tank(180, 30);

        DoubleBarrelTank t4 = new DoubleBarrelTank();
        TripleBarrelTank t5 = new TripleBarrelTank();

        t4.go(9, 9);
        t4.promote();

        // t1.go(1, 1);
        t2.damage(25);

        System.out.println(t1); // Tank
        System.out.println(t2); // Tank
        System.out.println(t3); // Tank

        System.out.println(t4); // DoubleBarrelTank
        System.out.println(t5); // TripleBarrelTank
    }
}
