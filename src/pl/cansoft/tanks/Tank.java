package pl.cansoft.tanks;

public class Tank {

    static int hpInitial = 1_000_000;
    static int fireInitial = 20;
    int x = 0;
    int y = 0;
    int barrel = 1;
    int hp = hpInitial;
    int fire = fireInitial;

    public Tank(int barrel) {
        this.barrel = barrel;
    }

    public Tank(int hp, int fire) {
        this.hp = hp;
        this.fire = fire;
    }

    public Tank(int x, int y, int hp) {
        this.x = x;
        this.y = y;
        this.hp = hp;
    }

    public Tank(int x, int y, int hp, int fire) {
        this.x = x;
        this.y = y;
        this.hp = hp;
        this.fire = fire;
    }

    final void go(int x, int y) {
        this.x = this.x + x;
        this.y = this.y + y;
    }

    void damage(int damage) {
        hp = hp - damage;
    }

    void promote() {
        fire = fire + 5;
        hp = hp + 10;
    }

    @Override
    public String toString() {
        return "Tank{" +
            "x=" + x +
            ", y=" + y +
            ", barrel=" + barrel +
            ", hp=" + hp +
            ", fire=" + fire +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tank tank = (Tank) o;

        if (x != tank.x) return false;
        if (y != tank.y) return false;
        if (barrel != tank.barrel) return false;
        if (hp != tank.hp) return false;
        return fire == tank.fire;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + barrel;
        result = 31 * result + hp;
        result = 31 * result + fire;
        return result;
    }
}
