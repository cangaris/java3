package pl.cansoft.cars;

public abstract class Vehicle {

    protected abstract void go();

    protected abstract void stop();
}
