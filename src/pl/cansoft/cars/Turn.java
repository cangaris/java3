package pl.cansoft.cars;

public interface Turn {

    void turnRight();

    void turnLeft();
}
