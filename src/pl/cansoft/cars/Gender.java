package pl.cansoft.cars;

public enum Gender {
    MALE,
    FEMALE,
    OTHER;

    public Gender getOther() {
        return OTHER;
    }
}
