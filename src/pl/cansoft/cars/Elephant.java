package pl.cansoft.cars;

public class Elephant implements Run, Turn, Eat {

    @Override
    public void eat() {

    }

    @Override
    public void go() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void turnRight() {

    }

    @Override
    public void turnLeft() {

    }
}
