package pl.cansoft.cars;

class Car implements Run, Turn, Charge {

    public void go() {
    }

    public void charge() {
    }

    public void stop() {
    }

    public void turnRight() {
    }

    public void turnLeft() {
    }
}
