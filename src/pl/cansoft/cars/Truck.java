package pl.cansoft.cars;

class Truck implements Run, Turn, Charge {

    public void stop() {
    }

    public void turnRight() {
    }

    public void turnLeft() {
    }

    @Override
    public void go() {
    }

    @Override
    public void charge() {
    }
}
