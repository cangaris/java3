package pl.cansoft.cars;

public interface Run {

    void go();

    void stop();
}
