package pl.cansoft.cars;

public class Main {

    public static void main(String[] args) {
        Gender g = Gender.OTHER;
        switch (g) {
            case MALE:
                System.out.println("Jesteś mężczyzną");
                break;
            case FEMALE:
                System.out.println("Jesteś kobietą");
                break;
            default:
                System.out.println("Niezdefinionano płci");
                break;
        }
    }
}
